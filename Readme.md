### What is this repository for? ###

BlackHoleOS is a Little and Simple Linux Like Operating System.

BlackHoleOS is build from ground up and contains a bit of the old NexOS Kernel (Developed by ATVG-Studios, the old kernel is build into the BHOS-MultiLive.iso image).

Version: BHOS-Alpha 0x05

### How do I get set up? ###

Download the "BHOS-MultiLive.iso" from the download section

Install Oracle VM Virutal Box

Create a Virtual Computer with:

- Select as Operating System "Other/Unknown" or "Other/Unknown (64-bit)" [I (AtjonTV) run all tests in a 64bit VM.]

- 64MB RAM

- 1 CPU

- 4MB VRAM

- No Hard Drive or 1 MB/GB

- (I Recomment to Disable Sound and Network, both are not needed in this Development stage.)

Now start the VM and test the OS (Its a Live CD, containing all versions of BlackHoleOS and the Kernel of NexOS on which BlackHoleOS was first build on)

### Contribution guidelines ###

If you try the System and find bugs, report them in BitBucket's Ticket System.

If you make your own version of BHOS, let us know about it, if we like it we might make a pull

### Who do I talk to? ###

You can Contact the Developer at dev.studio@atvg-studios.at

### Is there a License? ###
Yes. This Software is Licensed under the OSPL (Open Source Project License), for more inforation see the License in the Source Section: BlackHoleOS/License or here: http://www.atvg-studios.at/OSPLv1.1