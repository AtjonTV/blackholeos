/*
    BlackHoleOS Kernel
    (File: types.h )
    Written by AtjonTV (Thomas Obernosterer)
    Creation Date: 16.09.2017
    Last Edit Date: 16.09.2017
*/
#ifndef __TYPES_H
#define __TYPES_H

    typedef char int8_t;
    typedef unsigned char uint8_t;

    typedef short int16_t;
    typedef unsigned short uint16_t;

    typedef int int32_t;
    typedef unsigned int uint32_t;

    typedef long long int64_t;
    typedef unsigned long long uint64_t;

#endif