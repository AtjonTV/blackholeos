# Boot/Kernel Loader
#rm ./compiled/loader.o
#as --32 -o ./compiled/loader.o ./source/loader.s

# Interrupts ASM
#rm ./compiled/interruptstubs.o
#as --32 -o ./compiled/interruptstubs.o ./source/interruptstubs.s

# Kernel
#rm ./compiled/kernel.o
#gcc -m32 -c ./source/kernel.cpp -o ./compiled/kernel.o -Iinclude -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wno-write-strings

# GlobalDescriptorTable
#rm ./compiled/gdt.o
#gcc -m32 -c ./source/source/gdt.cpp -o ./compiled/gdt.o -Iinclude -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wno-write-strings

# Port Communication
#rm ./compiled/port.o
#gcc -m32 -c ./source/source/port.cpp -o ./compiled/port.o -Iinclude -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wno-write-strings

# Interrupts
#rm ./compiled/interrupts.o
#gcc -m32 -c ./source/source/interrupts.cpp -o ./compiled/interrupts.o -Iinclude -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wno-write-strings

# Kernel Binary
rm ./compiled/kernel.bin
ld -m elf_i386 -T ./source/linker.ld -o ./compiled/kernel.bin ./compiled/loader.o ./compiled/gdt.o ./compiled/port.o  ./compiled/interruptstubs.o ./compiled/interrupts.o ./compiled/kernel.o
